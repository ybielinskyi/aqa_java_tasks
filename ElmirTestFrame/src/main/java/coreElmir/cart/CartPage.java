package coreElmir.cart;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import coreElmir.BasePage;

@Getter
public class CartPage extends BasePage {

    public CartPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//span[@id='show-steps-text']")
    private WebElement checkoutButton;

    @FindBy(xpath = " //table[@class='tovars']")
    private WebElement cartItems; ///////////////Fix  //tr[contains(@id,'tr_tovar')][not(@class='hide-element')]


}
