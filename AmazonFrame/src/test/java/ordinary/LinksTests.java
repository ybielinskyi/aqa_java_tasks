package ordinary;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class LinksTests extends BaseTest{

    @Test(enabled = true)
    public void footerLinksTest() {
        try {
            for (WebElement element:homeHelper
                        .open()
                        .getPage()
                        .getFooterLinks()) {
                URL url = new URL(element.getAttribute("href"));

                HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
                httpConn.setConnectTimeout(2000);
                httpConn.connect();

                softAssert.assertEquals(httpConn.getResponseCode(), 200);

                httpConn.disconnect();
            }
        } catch (IOException ioException) {
            System.out.println("Invalid Link");
        }

        softAssert.assertAll();
    }

}
