package ordinary;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CartFeaturesTest extends BaseTest {

    @Test(enabled = true)
    public void endToEndTest() {
        Assert.assertEquals(homeHelper
                        .open()
                        .searchQuery("mouse")
                        .selectItem(1)
                        .addToCart()
                        .buyNowButtonClick()
                        .getPage()
                        .getWebDriver()
                        .getTitle(), "Amazon Sign-In");
    }

}
