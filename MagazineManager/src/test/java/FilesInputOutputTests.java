import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;

import static magazine.UtilityClass.readSavedMagazines;
import static magazine.UtilityClass.saveMagazines;
import static org.testng.Assert.assertEquals;

public class FilesInputOutputTests extends BaseTest {

    @Test
    public void outputTest() {
        File folder = new File(baseUrl);
        saveMagazines(magazinesList, baseUrl);

        assertEquals(folder.listFiles().length, 2);
    }

    @Test
    public void inputTest() {
        magazinesList = new ArrayList<>();
        magazinesList = readSavedMagazines(baseUrl);

        assertEquals(magazinesList.size(), 2);
    }
}
