package magazine.magazineCore;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Scanner;

@Getter
@Setter
@JsonAutoDetect
public class PaperMagazine extends Magazine {

    @JsonProperty("CountPage")
    private int countPage;

    @JsonProperty("WeightPage")
    private int weightPage;

    public PaperMagazine() {

    }

    public PaperMagazine(String type, String magazineName, String name, String language, double priceIssue, double pricePerYear, int frequency, Date issueDate, int countPage, int weightPage) {
        super(type, magazineName, name, language, priceIssue, pricePerYear, frequency, issueDate);

        this.countPage = countPage;
        this.weightPage = weightPage;
    }

    @Override
    public void editIssue() {
        System.out.println("Enter new issue name:");
        String issueName = new Scanner(System.in).nextLine();

        setIssueName(issueName);
        setLanguage("new language");
        setPriceIssue(12);
        setPricePerYear(120);
        setFrequency(2);
    }

    @Override
    public String toString() {
        return "Type = ".concat(type).concat(", Name = ").concat(IssueName).concat(", Language = ").concat(language).concat(", priceIssue = ")
                .concat(String.valueOf(priceIssue)).concat(", frequency = ").concat(String.valueOf(frequency)).concat(", Issue date = ")
                .concat(String.valueOf(issueDate)).concat(", Page count = ").concat(String.valueOf(countPage)).concat(", Page weight = ")
                .concat(String.valueOf(weightPage));
    }
}