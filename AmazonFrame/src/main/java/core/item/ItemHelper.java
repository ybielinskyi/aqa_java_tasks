package core.item;

import core.cart.CartHelper;
import core.BaseHelper;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

public class ItemHelper extends BaseHelper<ItemPage> {

    public ItemHelper(WebDriver webDriver) {
        super(new ItemPage(webDriver));
    }

    public CartHelper addToCart() {
        getPage().getAddToCartButton().click();

        return (new CartHelper(getPage().getWebDriver()));
    }

}
