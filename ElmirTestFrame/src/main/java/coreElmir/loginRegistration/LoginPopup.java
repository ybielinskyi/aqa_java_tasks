package coreElmir.loginRegistration;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import coreElmir.BasePage;

@Getter
public class LoginPopup extends BasePage {

    public LoginPopup(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//input[@placeholder='Логин']")
    private WebElement loginField;

    @FindBy(xpath = "//input[@placeholder='Пароль']")
    private WebElement passField;

    @FindBy(xpath = "//button[@class='button']")
    private WebElement loginButton;

    public LoginPopup enterLogin(String login) {
        getLoginField().sendKeys(login);

        return this;
    }

    public LoginPopup enterPass(String pass) {
        getPassField().sendKeys(pass);

        return this;
    }

    public LoginPopup loginButtonClick() {
        getLoginButton().click();

        return this;
    }

}
