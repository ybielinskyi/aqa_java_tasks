package coreElmir.loginRegistration;

import coreElmir.BaseHelper;
import org.openqa.selenium.WebDriver;

public class RegistrationHelper extends BaseHelper<RegistrationPage> {

    public RegistrationHelper(WebDriver webDriver) {
        super(new RegistrationPage(webDriver));
    }

    public RegistrationHelper enterEmail(String email) {
        getPage().getEmailField().sendKeys(email);

        return this;
    }

    public RegistrationHelper enterPass(String pass) {
        getPage().getPassField().sendKeys(pass);

        return this;
    }

    public RegistrationHelper clickRegistrationButton() {
        getPage().getRegistrationButton().click();

        return this;
    }

}
