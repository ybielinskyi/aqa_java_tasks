import magazine.magazineCore.Magazine;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static magazine.UtilityClass.readSavedMagazines;


public class SerializationMagazines {

    private static String baseUrl = "MagazineManager/src/main/resources/magazines/";
    private static List<Magazine> magazineList = new ArrayList<>();

    public static void main(String[] args) {
        magazineList = readSavedMagazines(baseUrl);

        serialize(magazineList);
        magazineList = deserialize();

        System.out.println(magazineList);
    }

    public static void serialize(List<Magazine> magazineList) {
        try {
            FileOutputStream fos = new FileOutputStream("magazineData");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(magazineList);
            oos.close();
            fos.close();
        } catch (IOException ioe) {
            System.out.println("Cannot serialize");
            ioe.printStackTrace();
        }
    }

    public static List<Magazine> deserialize() {
        List<Magazine> magazineList = new ArrayList<>();

        try
        {
            FileInputStream fis = new FileInputStream("magazineData");
            ObjectInputStream ois = new ObjectInputStream(fis);

            magazineList = (List<Magazine>) ois.readObject();

            ois.close();
            fis.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }

        return magazineList;
    }
}
