package summator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FloatSummator implements Sumator<Float> {

    @Override
    public Map<String, Double> sum(Map<String, List<Float>> data) {
        Map<String, Double> map = new HashMap<String, Double>();

        for (Map.Entry entry:data.entrySet()) {
            Double summ = 0.0;

            for (Float f:(List<Float>) entry.getValue()) {
                summ+= f;
            }

            map.put(entry.getKey().toString(), summ);
        }

        return map;
    }
}
