import calculator.Calculate;
import com.sun.tools.javac.util.Convert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class NegativeTests {
    @Test
    public void symbolsInputTest() {
        assertEquals(Calculate.evaluate("!@#1+2*&"), "1.0 + 2.0 = 3.0");
    }

    @Test
    public void invalidOperand() {
        assertEquals(Calculate.evaluate("1++++++2"), "Invalid operand");
    }

    @Test
    public void alphabeticInput() {
        assertEquals(Calculate.evaluate("asd1+2asd"), "1.0 + 2.0 = 3.0");
    }

    @Test
    public void divisionZero() {
        assertEquals(Calculate.evaluate("0/0"), "0.0 / 0.0 = NaN");
    }
}
