public class Main {

    public static String simple(int start, int end) {
        if (start > end) {
            return simple(end, start);
        }

        String result = "";

        for (int i = start; i <= end; i++) {
            for (int j = 1, k = 0; j <= i; j++) {
                if (i % j == 0) {
                    k++;
                }
                if (j == i && k <= 2) {
                    result +=  i + " ";
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(simple(10, 1));
    }
}
