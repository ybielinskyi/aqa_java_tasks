package core.item;

import core.BasePage;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Getter
public class ItemPage extends BasePage {

    public ItemPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//span[@id='productTitle']")
    private WebElement titleItem;

    @FindBy(xpath = "//input[@id='add-to-cart-button']")
    private WebElement addToCartButton;

}
