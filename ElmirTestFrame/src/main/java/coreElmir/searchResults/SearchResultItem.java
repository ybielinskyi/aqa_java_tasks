package coreElmir.searchResults;

import coreElmir.BasePage;
import coreElmir.cart.CartHelper;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Getter
public class SearchResultItem extends BasePage {

    public SearchResultItem(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//a[text()='Добавить в корзину']")
    private WebElement addToCartButton;

    @FindBy(css = "p[class='name']")
    private WebElement itemName;

    @FindBy(xpath = "//span[@id='basket-text']")
    private WebElement cart;

    public CartHelper addItemToCart() {
        getAddToCartButton().click();
        getCart().click();

        return (new CartHelper(getWebDriver()));
    }
}
