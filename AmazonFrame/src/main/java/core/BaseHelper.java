package core;

import lombok.AccessLevel;
import lombok.Getter;

@Getter
public class BaseHelper<TPage extends BasePage> {

    private TPage page;

    public BaseHelper(TPage page) {
        this.page = page;
    }
}
