package cucmber;

import configuration.WebDriverSingleton;
import core.cart.CartHelper;
import core.home.HomeHelper;
import core.item.ItemHelper;
import core.search.SearchResultsHelper;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@CucumberOptions(
        features = "src/test/java/cucmber/amazon.feature",
        glue = {"cucmber"})
public class TestRunner extends AbstractTestNGCucumberTests {

    @Autowired
    public static WebDriver webDriver;
    public static HomeHelper homeHelper;
    public static SearchResultsHelper searchResultsHelper;
    public static ItemHelper itemHelper;
    public static CartHelper cartHelper;

    @PostConstruct
    @BeforeClass
    public void beforeSuiteSetUp() {
        webDriver = WebDriverSingleton.getInstance();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        homeHelper = new HomeHelper(webDriver);
        searchResultsHelper = new SearchResultsHelper(webDriver);
        itemHelper = new ItemHelper(webDriver);
        cartHelper = new CartHelper(webDriver);
    }

    @AfterClass
    public void afterSuiteTearDown() {
        webDriver.quit();

    }
}
