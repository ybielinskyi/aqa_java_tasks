import org.testng.annotations.Test;

import static magazine.UtilityClass.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CollectionOperationTest extends BaseTest {

    @Test
    public void printMagazinesTest() {
        deleteIssue(magazinesList, getIssueByMagazineIssueName(magazinesList, "paperMagazine/testMagazine"));
        assertEquals(magazinesList.size(), 1);
    }

    @Test
    public void fillCollectionTest() {
        assertEquals(magazinesList.size(), 2);
    }

    @Test
    public void sortDescendingTest() {
        sortDescending(magazinesList);
        assertTrue(magazinesList.get(0).getIssueDate().before(magazinesList.get(magazinesList.size() - 1).getIssueDate()));
    }

    @Test
    public void sortAscendingTest() {
        sortAscending(magazinesList);
        assertTrue(magazinesList.get(0).getIssueDate().after(magazinesList.get(magazinesList.size() - 1).getIssueDate()));
    }
}
