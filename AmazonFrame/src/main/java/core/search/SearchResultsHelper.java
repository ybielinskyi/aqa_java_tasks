package core.search;

import core.BaseHelper;
import core.item.ItemHelper;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

public class SearchResultsHelper extends BaseHelper<SearchResultsPage> {

    public SearchResultsHelper(WebDriver webDriver) {
        super(new SearchResultsPage(webDriver));
    }

    public ItemHelper selectItem(int index) {
        getPage().getResultItems().get(index).click();

        return (new ItemHelper(getPage().getWebDriver()));
    }
}
