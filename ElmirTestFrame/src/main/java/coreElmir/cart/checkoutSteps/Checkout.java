package coreElmir.cart.checkoutSteps;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import coreElmir.BasePage;

@Getter
public class Checkout extends BasePage {

    public Checkout(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//input[@id='payment_0']")
    private WebElement cashPayment;

    @FindBy(xpath = "//button[@id='next-step-3']")
    private WebElement checkDataButton;

    public CheckData fillCheckoutRequiredFields() {
        getCashPayment().click();
        getCheckDataButton().click();

        return (new CheckData(webDriver));
    }

}
