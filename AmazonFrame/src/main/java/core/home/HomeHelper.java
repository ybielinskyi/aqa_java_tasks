package core.home;

import configuration.Config;
import core.BaseHelper;
import core.search.SearchResultsHelper;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

public class HomeHelper extends BaseHelper<HomePage> {

    public HomeHelper(WebDriver webDriver) {
        super(new HomePage(webDriver));
    }

    public HomeHelper() {
        super(new HomePage());
    }

    public HomeHelper open() {
        getPage().getWebDriver().get(Config.property.getProperty("baseURL"));

        return this;
    }

    public SearchResultsHelper searchQuery(String query) {
        getPage().getSearchField().sendKeys(query);
        getPage().getSearchButton().click();

        return (new SearchResultsHelper(getPage().getWebDriver()));
    }
}
