package core.cart;

import core.BaseHelper;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

public class  CartHelper extends BaseHelper<CartPage> {

    public CartHelper(WebDriver webDriver) {
        super(new CartPage(webDriver));
    }

    public CartHelper buyNowButtonClick() {
        getPage().getBuyNowButton().click();

        return this;
    }
}
