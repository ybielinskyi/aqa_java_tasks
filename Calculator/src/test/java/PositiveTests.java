import calculator.Calculate;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PositiveTests {
    @Test
    public void additionTest() {
        assertEquals(Calculate.evaluate("1+2"), "1.0 + 2.0 = 3.0");
    }

    @Test
    public void divisionTest() {
        assertEquals(Calculate.evaluate("1/2"), "1.0 / 2.0 = 0.5");
    }

    @Test
    public void multiplicationTest() {
        assertEquals(Calculate.evaluate("3*2"), "3.0 * 2.0 = 6.0");
    }

    @Test
    public void subtractionTest() {
        assertEquals(Calculate.evaluate("1-2"), "1.0 - 2.0 = -1.0");
    }
}
