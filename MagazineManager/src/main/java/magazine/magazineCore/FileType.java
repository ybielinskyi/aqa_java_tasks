package magazine.magazineCore;

public enum FileType {
    TXT,
    PDF
}