import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class VampireNumTests {

    @Test
    public void validNotVampireNum() {
        assertEquals(Main.vampireNum(1234), "");
    }

    @Test
    public void validVampireNum() {
        assertEquals(Main.vampireNum(1260), "21 * 60 = 1260");
    }

    @Test
    public void lowerLowLimitNum() {
        assertEquals(Main.vampireNum(10000), "Invalid input. OutOfRange (999 < x <= 9999)");

    }

    @Test
    public void upperUpLimitNum() {
        assertEquals(Main.vampireNum(999), "Invalid input. OutOfRange (999 < x <= 9999)");
    }

}
