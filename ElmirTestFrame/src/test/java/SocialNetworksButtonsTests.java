import org.testng.Assert;
import org.testng.annotations.Test;

public class SocialNetworksButtonsTests extends BaseTest {

    @Test
    public void telegramButtonTest() throws InterruptedException {
        Assert.assertEquals(homeHelper
                .open()
                .telegramButtonClick()
                .getPage()
                .getWebDriver()
                .getCurrentUrl(), "https://t.me/Elmir_UA_official");
    }

    @Test
    public void vkontakteButtonTest() {
        Assert.assertEquals(homeHelper
                .open()
                .vkontakteButtonClick()
                .getPage()
                .getWebDriver()
                .getCurrentUrl(), "https://vk.com/ua_elmir");

    }

    @Test
    public void youtubeButtonTest() {
        Assert.assertEquals(homeHelper
                .open()
                .youtubeButtonClick()
                .getPage()
                .getWebDriver()
                .getCurrentUrl(), "https://www.youtube.com/user/elmirua");

    }

    @Test
    public void facebookButtonTest() {
        Assert.assertEquals(homeHelper
                .open()
                .facebookButtonClick()
                .getPage()
                .getWebDriver()
                .getCurrentUrl(), "https://www.facebook.com/elmir.ua");

    }

    @Test
    public void instagramButtonTest() {
        Assert.assertEquals(homeHelper
                .open()
                .instagramButtonClick()
                .getPage()
                .getWebDriver()
                .getCurrentUrl(), "https://www.instagram.com/elmir.ua/");

    }

}
