package calculator.operations.core;

public interface Operation {

    char name();

    double execute(double firstOperand, double secondOperand);
}
