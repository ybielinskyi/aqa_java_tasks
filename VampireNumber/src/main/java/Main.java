public class Main {

    public static String vampireNum(int num) {
        if (num <= 999 || num > 9999) return "Invalid input. OutOfRange (999 < x <= 9999)";
        String[] numToStr = Integer.toString(num).split("");

        int[] pairs = new int[16];

        for (int i = 0, k = 0; i < 4; i++){
            for (int j = 0; j < 4; j++, k++) {
                if(!numToStr[i].equals(numToStr[j])) pairs[k] = Integer.parseInt(numToStr[i] + numToStr[j]);
            }
        }

        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                if (pairs[i] * pairs[j] == num) return (pairs[i] + " * " + pairs[j] + " = " + num);
            }
        }

        return "";
    }

    public static void main(String[] args) {
        for (int i = 1000; i <= 9999; i++) {
            if (!vampireNum(i).equals("")) {
                System.out.println(vampireNum(i));
            }
        }
    }
}
