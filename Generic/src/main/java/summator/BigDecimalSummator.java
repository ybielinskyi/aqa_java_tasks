package summator;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BigDecimalSummator implements Sumator<BigDecimal> {

    @Override
    public Map<String, Double> sum(Map<String, List<BigDecimal>> data) {
        Map<String, Double> map = new HashMap<>();

        for (Map.Entry entry:data.entrySet()) {
            BigDecimal summ = new BigDecimal(0.0);

            for (BigDecimal bigDecimal:(List<BigDecimal>) entry.getValue()) {
                summ = summ.add(bigDecimal);
            }

            map.put(entry.getKey().toString(), summ.doubleValue());
        }

        return map;
    }

}
