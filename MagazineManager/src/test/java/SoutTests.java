import magazine.UtilityClass;
import org.testng.annotations.Test;

import static magazine.UtilityClass.printMagazinesIssuesNames;
import static org.testng.Assert.assertEquals;

public class SoutTests extends BaseTest {

    @Test(groups = "System.out.println")
    public void printMagazinesIssuesNamesInfoTest() {
        printMagazinesIssuesNames(magazinesList);

        assertEquals("paperMagazine/testMagazine\n" + "eMagazine/testMagazine\n", outContent.toString());
    }

    @Test(groups = "System.out.println")
    public void printMagazinesTest() {
        UtilityClass.printMagazines(magazinesList);

        assertEquals("paperMagazine\n" + "eMagazine\n", outContent.toString());
    }

}
