package coreElmir.cart.checkoutSteps;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import coreElmir.BasePage;

@Getter
public class Delivery extends BasePage {

    public Delivery(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//input[@id='dt-1']")
    private WebElement selfRadioButton;

    @FindBy(xpath = "//input[@id='post_service_2_1']")
    private WebElement elmirShop;

    @FindBy(xpath = "//input[@id='post_4687']")
    private WebElement firstAddress;

    @FindBy(xpath = "//button[@id='next-step-2']")
    private WebElement checkoutButton;

    public Checkout fillDeliveryRequiredFields() {
        getSelfRadioButton().click();
        getElmirShop().click();
        getFirstAddress().click();
        getCheckoutButton().click();

        return (new Checkout(webDriver));
    }
}
