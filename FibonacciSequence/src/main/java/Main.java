public class Main {
    public static String sequence(int endSequence) {
        if (endSequence <= 0) {
            return "Invalid endSequence";
        }

        int[] sequence = {1, 1};
        String result = "1 1 ";

        while (sequence[0] + sequence[1] <= endSequence) {
            int tmp = sequence[0];
            sequence[0] = sequence[1];
            sequence[1] = sequence[0] + tmp;

            result += sequence[1] + " ";
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(sequence(14));
    }
}
