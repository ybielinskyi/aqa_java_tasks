package converter;

public class ConvertToString implements Converter<String, Integer[]> {

    @Override
    public String get(Integer[] value) {
        String result = value[0].toString();

        for (int i = 1; i < value.length; i++) {
            result += (' ' + value[i].toString());
        }

        return result;
    }
}
