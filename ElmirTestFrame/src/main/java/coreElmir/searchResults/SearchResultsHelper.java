package coreElmir.searchResults;

import coreElmir.cart.CartHelper;
import coreElmir.BaseHelper;
import org.openqa.selenium.WebDriver;

public class SearchResultsHelper extends BaseHelper<SearchResultsPage> {

    public SearchResultsHelper(WebDriver webDriver) {
        super(new SearchResultsPage(webDriver));
    }

    public CartHelper addToCart() {
        getPage().getItems().get(0).getAddToCartButton().click();

        return (new CartHelper(getPage().getWebDriver()));
    }

}
