package calculator.operations;

import calculator.operations.core.Operation;

public class Division implements Operation {

    public char name() {
        return '/';
    }

    public double execute(double firstOperand, double secondOperand) {
        return (firstOperand / secondOperand);
    }
}
