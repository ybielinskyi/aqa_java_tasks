package coreElmir.home;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import coreElmir.BasePage;

@Getter
public class HomePage extends BasePage {

    public HomePage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//span[@id='basket-text']")
    private WebElement cart;

    @FindBy(css = "#q")
    private WebElement searchField;

    @FindBy(css = "#find")
    private WebElement searchButton;

    @FindBy(xpath = "//span[@class='auth-btn shower']")
    private WebElement loginButton;

    //Social networks buttons
    @FindBy(xpath = "//div/a[@class='telegram']")
    private WebElement telegramButton;

    @FindBy(xpath = "//a[@class='vkontakte']")
    private WebElement vkontakteButton;

    @FindBy(xpath = "//a[@class='youtube']")
    private WebElement youtubeButton;

    @FindBy(xpath = "//a[@class='facebook']")
    private WebElement facebookButton;

    @FindBy(xpath = "//a[@class='instagram']")
    private WebElement instagramButton;

}
