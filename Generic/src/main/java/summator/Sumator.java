package summator;

import java.util.List;
import java.util.Map;

public interface Sumator<T extends Number> {

    Map<String, Double> sum(Map<String, List<T>> data);
}
