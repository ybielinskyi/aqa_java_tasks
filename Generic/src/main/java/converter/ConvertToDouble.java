package converter;

public class ConvertToDouble implements Converter<Double, Float> {

    @Override
    public Double get(Float value) {
        return value.doubleValue();
    }
}
