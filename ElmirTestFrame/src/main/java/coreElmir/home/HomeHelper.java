package coreElmir.home;

import coreElmir.BaseHelper;
import org.openqa.selenium.WebDriver;
import coreElmir.searchResults.SearchResultsPage;
import coreElmir.loginRegistration.LoginPopup;
import configuration.Config;

import java.util.ArrayList;

public class HomeHelper extends BaseHelper<HomePage> {

    public HomeHelper(WebDriver webDriver) {
        super(new HomePage(webDriver));
    }

    public HomeHelper open() {
        getPage().getWebDriver().get(Config.property.getProperty("baseURL"));

        return this;
    }

    public SearchResultsPage searchQuery(String query) {
        getPage().getSearchField().sendKeys(query);
        getPage().getSearchButton().click();

        return (new SearchResultsPage(getPage().getWebDriver()));
    }

    public LoginPopup loginButtonClick() {
        getPage().getLoginButton().click();

        return (new LoginPopup(getPage().getWebDriver()));
    }

    public HomeHelper telegramButtonClick() throws InterruptedException {
        getPage()
                .getTelegramButton()
                .click();

        ArrayList tabs = new ArrayList (getPage().getWebDriver().getWindowHandles());

        getPage()
                .getWebDriver()
                .switchTo()
                .window(tabs.get(1).toString());

        return this;
    }

    public HomeHelper vkontakteButtonClick() {
        getPage()
                .getVkontakteButton()
                .click();

        ArrayList tabs = new ArrayList (getPage().getWebDriver().getWindowHandles());

        getPage()
                .getWebDriver()
                .switchTo()
                .window(tabs.get(1).toString());

        return this;
    }

    public HomeHelper facebookButtonClick() {
        getPage()
                .getFacebookButton()
                .click();

        ArrayList tabs = new ArrayList (getPage().getWebDriver().getWindowHandles());

        getPage()
                .getWebDriver()
                .switchTo()
                .window(tabs.get(1).toString());

        return this;
    }

    public HomeHelper instagramButtonClick() {
        getPage()
                .getInstagramButton()
                .click();

        ArrayList tabs = new ArrayList (getPage().getWebDriver().getWindowHandles());

        getPage()
                .getWebDriver()
                .switchTo()
                .window(tabs.get(1).toString());

        return this;
    }

    public HomeHelper youtubeButtonClick() {
        getPage()
                .getYoutubeButton()
                .click();

        ArrayList tabs = new ArrayList (getPage().getWebDriver().getWindowHandles());

        getPage()
                .getWebDriver()
                .switchTo()
                .window(tabs.get(1).toString());

        return this;
    }
}
