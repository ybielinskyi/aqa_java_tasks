package coreElmir.cart.checkoutSteps;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import coreElmir.BasePage;

@Getter
public class CheckData extends BasePage {

    public CheckData(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//button[@id='next-step-4']")
    private WebElement sendOrderButton;


}
