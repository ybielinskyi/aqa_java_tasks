package calculator;

import calculator.operations.Addition;
import calculator.operations.Division;
import calculator.operations.Multiplication;
import calculator.operations.Subtraction;
import calculator.operations.core.Operation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Calculate {
    private static List<Operation> operations = new ArrayList<Operation>();
    private static String inputString;
    private static double firstOperand;
    private static double secondOperand;
    private static String[] operands;

    private static void init(String input) {
        Collections.addAll(operations, new Addition(), new Division(), new Multiplication(), new Subtraction());

        inputString = input.replaceAll("[^0-9./+*-]", "");
        operands =  inputString.split("[-/+*]");
        inputString = inputString.replaceAll("[^/+*-]", "");
    }

    public static String evaluate(String input) {
        init(input);

        try {
            firstOperand = Double.parseDouble(operands[0]);
            secondOperand = Double.parseDouble(operands[1]);
        }
        catch (NumberFormatException ex) {
            System.out.println("Invalid operand");
            return "Invalid operand";
        }

        for (Operation operation:operations) {
            if (operation.name() == inputString.toCharArray()[0]) {
                System.out.println(firstOperand + " " + inputString.toCharArray()[0] + " " + secondOperand + " = " + operation.execute(firstOperand, secondOperand));
                return (firstOperand + " " + inputString.toCharArray()[0] + " " + secondOperand + " = " + operation.execute(firstOperand, secondOperand));
            }
        }
        return "Unexpected error";
    }
}
