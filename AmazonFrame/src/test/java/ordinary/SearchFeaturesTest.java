package ordinary;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchFeaturesTest extends BaseTest {

    @Test(enabled = true)
    public void searchQueryTest() {
        Assert.assertTrue(homeHelper
                .open()
                .searchQuery("mouse")
                .getPage()
                .getSearchQuery()
                .getText()
                .contains("mouse"));

    }

    @Test(enabled = true)
    public void searchFeaturesTest() {
        Assert.assertTrue(homeHelper
                .open()
                .searchQuery("mouse")
                .selectItem(0)
                .getPage()
                .getTitleItem()
                .getText()
                .toLowerCase()
                .contains("mouse"));
    }

}
