import coreElmir.home.HomeHelper;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import configuration.WebDriverSingleton;
import coreElmir.searchResults.SearchResultsPage;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected static WebDriver webDriver;
    protected static HomeHelper homeHelper;
    protected static SearchResultsPage searchResultsPage;

    @BeforeClass
    public void setUpBeforeClass() {
        webDriver = WebDriverSingleton.getInstance();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

    }

    @BeforeMethod
    public void setUpBeforeMethod() {
        homeHelper = new HomeHelper(webDriver);
        searchResultsPage = new SearchResultsPage(webDriver);
    }

    @AfterMethod
    public void tearDownAfterMethod() {
        String current = webDriver.getWindowHandle();

        for(String tab : webDriver.getWindowHandles())
            if (!tab.equals(current)) webDriver.switchTo().window(tab).close();

        webDriver.switchTo().window(current);

    }

    @AfterClass
    public void tearDownAfterClass() {
        webDriver.quit();
    }
}
