package magazine;

import com.fasterxml.jackson.databind.ObjectMapper;
import magazine.magazineCore.EMagazine;
import magazine.magazineCore.Magazine;
import magazine.magazineCore.PaperMagazine;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class UtilityClass {

    private static Scanner scanner = new Scanner(System.in);

    public static void printMagazines(List<Magazine> magazineList) {
        for (Magazine magazine : magazineList) {
            System.out.println(magazine.getMagazineName());
        }
    }

    public static void printMagazineIssues(List<Magazine> magazineList, String magazineName) {
        for (Magazine magazine : magazineList) {
            if (magazine.getMagazineName().equals(magazineName)) System.out.println(magazine.getIssueName());
        }
    }

    public static void saveMagazines(List<Magazine> magazineList, String baseUrl) {
        try {
            FileUtils.deleteDirectory(new File(baseUrl));

            ObjectMapper mapper = new ObjectMapper();

            for (Magazine magazine : magazineList) {
                File theDir = new File(baseUrl + magazine.getType() + '/' + magazine.getMagazineName());
                if (!theDir.exists()) FileUtils.forceMkdir(theDir);

                mapper.writeValue(new File(baseUrl + magazine.getType() + '/' + magazine.getMagazineName() + '/' + magazine.getIssueName() + ".json"), magazine);
            }
        } catch (IOException e) {
            System.out.println("Cannot save files!");
        }
    }

    public static List<Magazine> readSavedMagazines(String baseUrl) {
        List<Magazine> magazineList = new LinkedList<>();
        File folder = new File(baseUrl);
        File[] folderEntries = folder.listFiles();

        for (File entry : folderEntries) {
            if (entry.isDirectory()) {
                magazineList.addAll(readSavedMagazines(String.valueOf(entry)));
                continue;
            }
            try {
                ObjectMapper mapper = new ObjectMapper();
                magazineList.add(mapper.readValue(entry, Magazine.class));
            } catch (IOException e) {
                System.out.println("Cannot read from file " + entry.getName());
            }

        }

        return magazineList;
    }

    public static void printMagazinesInfo(List<Magazine> magazineList) {
        for (Magazine magazine : magazineList) {
            System.out.println(magazine.toString());
        }
    }

    public static void printMagazinesIssuesNames(List<Magazine> magazineList) {
        for (Magazine magazine : magazineList) {
            System.out.println(magazine.getMagazineName() + '/' + magazine.getIssueName());
        }
    }

    public static void printIssueInfo(Magazine magazine) {
        System.out.println(magazine.toString());
    }

    public static Magazine getIssueByMagazineIssueName(List<Magazine> magazineList, String magazineIssueName) {
        for (Magazine magazine : magazineList) {
            if ((magazine.getMagazineName() + '/' + magazine.getIssueName()).equals(magazineIssueName)) return magazine;
        }

        return null;
    }

    public static void addIssue(List<Magazine> magazineList) {
        System.out.println("Enter [MagazineName, issueName]:");
        String magazineName = scanner.nextLine();
        String issueName = scanner.nextLine();

        System.out.println("Enter magazine type[paperMagazine, eMagazine]:");
        String type = scanner.nextLine();

        switch (type) {
            case "paperMagazine":
                magazineList.add(new PaperMagazine(type, magazineName, issueName, "eng", 0, 0, 1, new Date(1999 - 10 - 12), 123, 12));
                break;

            case "eMagazine":
                magazineList.add(new EMagazine(type, magazineName, issueName, "eng", 0, 0, 1, new Date(2015 - 9 - 18), "PDF", 123));
                break;

        }

    }

    public static void editIssue(Magazine magazine) {
        magazine.editIssue();
    }

    public static void sortDescending(List<Magazine> magazineList) {
        magazineList.sort(Comparator.comparing(Magazine::getIssueDate));
    }

    public static void sortAscending(List<Magazine> magazineList) {
        magazineList.sort(Comparator.comparing(Magazine::getIssueDate).reversed());
    }

    public static void deleteIssue(List<Magazine> magazineList, Magazine magazine) {
        magazineList.remove(magazine);
    }

    public static void editMagazineName(Magazine magazine, String magazineName) {
        magazine.setMagazineName(magazineName);
    }

}