package converter;

public interface Converter<T,I> {

    T get(I value);
}
