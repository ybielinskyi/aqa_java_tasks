Feature: Amazon Test

  Scenario: search amazon.com
    Given I go to amazon main page
    When I query for "mouse"
    Then amazon page title should become "Amazon.com: mouse"

  Scenario: search amazon.com and go item page
    Given I go to amazon main page
    And I query for "mouse"
    When I click on first item
    Then amazon page title should contain "mouse"

  Scenario: add to cart test
    Given I go to amazon main page
    And I query for "mouse"
    And I click on first item
    When I click buyNow button
    Then amazon page title should become "Amazon Sign-In"
