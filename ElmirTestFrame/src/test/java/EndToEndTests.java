import org.testng.Assert;
import org.testng.annotations.Test;

public class EndToEndTests extends BaseTest {

    @Test
    public void endToEndTest() {
        Assert.assertTrue(homeHelper
                .open()
                .searchQuery("mouse")
                .getItems()
                .get(0)
                .addItemToCart()
                .checkOutButtonClick()
                .fillQuestionnaireRequiredFields()
                .fillDeliveryRequiredFields()
                .fillCheckoutRequiredFields()
                .getSendOrderButton()
                .isEnabled());
    }
}
