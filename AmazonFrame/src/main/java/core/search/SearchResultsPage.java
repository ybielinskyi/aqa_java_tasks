package core.search;

import core.BasePage;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

@Getter
public class SearchResultsPage extends BasePage {

    public SearchResultsPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//span[@class='a-color-state a-text-bold']")
    private WebElement searchQuery;

    @FindBy(xpath = "//div[@class='s-include-content-margin s-border-bottom']//a[@class='a-link-normal a-text-normal']")
    private List<WebElement> resultItems;

}
