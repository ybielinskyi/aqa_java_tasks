package coreElmir.cart.checkoutSteps;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import coreElmir.BasePage;

@Getter
public class Questionnaire extends BasePage {

    public Questionnaire(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//input[@id='surname']")
    private WebElement surnameFiled;

    @FindBy(xpath = "//input[@id='name']")
    private WebElement nameField;

    @FindBy(xpath = "/input[@id='phone']")
    private WebElement phoneField;

    @FindBy(xpath = "/button[@id='next-step-1']")
    private WebElement deliveryButton;

    public Delivery fillQuestionnaireRequiredFields() {
        getNameField().sendKeys("Иван");
        getSurnameFiled().sendKeys("Самойленко");
        getPhoneField().sendKeys("0999287717");
        getDeliveryButton().click();

        return (new Delivery(webDriver));
    }

}
