package cucmber;

import core.BasePage;
import core.cart.CartHelper;
import core.home.HomeHelper;
import core.search.SearchResultsHelper;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;

import static cucmber.TestRunner.*;

public class AmazonStepDef extends BaseStep {

    @When("I query for {string}")
    public void iQueryFor(String query) {
        homeHelper.searchQuery(query);
    }

    @Then("amazon page title should become {string}")
    public void googlePageTitleShouldBecome(String query) {
        Assert.assertEquals(homeHelper.getPage().getWebDriver().getTitle(), query);
    }

    @When("I click on first item")
    public void iClickOnFirstItem() {
        searchResultsHelper.selectItem(0);
    }

    @Then("amazon page title should contain {string}")
    public void amazonPageTitleShouldContain(String query) {
        Assert.assertTrue(searchResultsHelper.getPage().getWebDriver().getTitle().toLowerCase().contains(query));
    }

    @When("I click buyNow button")
    public void iClickBuyNowButton() {
        cartHelper.buyNowButtonClick();
    }

    @Given("I go to amazon main page")
    public void iGoToAmazonMainPage() {
        homeHelper.open();
    }
}
