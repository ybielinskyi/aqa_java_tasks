import org.testng.Assert;
import org.testng.annotations.Test;

public class CartTests extends BaseTest {

    @Test
    public void cartFeaturesTest() {
        Assert.assertTrue(homeHelper
                .open()
                .searchQuery("mouse")
                .getItems()
                .get(0)
                .addItemToCart()
                .getCartItems()
                .getText()
                .contains("mouse"));
    }
}
