import magazine.magazineCore.Magazine;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static magazine.UtilityClass.*;

public class MagazineManager {

    private static Scanner scanner = new Scanner(System.in);
    private static String baseUrl = "MagazineManager/src/main/resources/magazines/";
    private static List<Magazine> magazineList = new ArrayList<>();
    private static boolean running = true;

    public static void main(String[] args) {
        magazineList = readSavedMagazines(baseUrl);

        while (running) {
            controller();
        }

        saveMagazines(magazineList, baseUrl);
    }

    public static void controller() {
        System.out.println("ADMIN PERMISSION");
        System.out.println("0 - EXIT");
        System.out.println("1.1 - Delete particular issues of magazine, 1.2 - Print all Magazines/Issues");
        System.out.println("2.1 - Add new issue/magazine");
        System.out.println("3.1 - Modify magazine properties, 3.2 - Modify issue content");
        System.out.println("4.1 - Descending sort, 4.2 - Ascending sort");
        System.out.println("5.1 - Get content of all magazines issued in particular month");
        System.out.println("6.1 - Calculate discount for year subscription in comparison with paying for each issue separately");
        String decision = scanner.nextLine();

        switch (decision) {
            case "0":
                running = false;
                break;

            case "1.1":
                System.out.println("All issue:");
                printMagazinesIssuesNames(magazineList);
                System.out.println("Issue to delete:");
                String issueName = scanner.nextLine();
                deleteIssue(magazineList, getIssueByMagazineIssueName(magazineList, issueName));
                break;

            case "1.2":
                System.out.println("All magazines");
                printMagazinesIssuesNames(magazineList);
                break;

            case "2.1":
                System.out.println("Adding new issue/magazine");
                addIssue(magazineList);
                break;

            case "3.1":
                System.out.println("Modify magazine properties");
                printMagazinesIssuesNames(magazineList);
                System.out.println("Enter [MagazineName/IssueName, new MagazineName]:");
                String magazineName = scanner.nextLine();
                String newName = scanner.nextLine();
                editMagazineName(getIssueByMagazineIssueName(magazineList, magazineName), newName);
                break;

            case "3.2":
                System.out.println("Modify issue properties");
                System.out.println("Enter issue:");
                printMagazinesIssuesNames(magazineList);
                issueName = scanner.nextLine();
                editIssue(getIssueByMagazineIssueName(magazineList, issueName));
                break;

            case "4.1":
                System.out.println("Descending sort:");
                sortDescending(magazineList);
                break;

            case "4.2":
                System.out.println("Ascending sort:");
                sortAscending(magazineList);
                break;

            case "6.1":
                System.out.println("Calculate discount");
                printMagazinesIssuesNames(magazineList);
                issueName = scanner.nextLine();
                magazineList.get(magazineList.indexOf(getIssueByMagazineIssueName(magazineList, issueName))).calculateDiscount();
                break;
        }
    }
}
