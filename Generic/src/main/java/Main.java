import converter.ConvertToDouble;
import converter.ConvertToString;
import summator.BigDecimalSummator;
import summator.FloatSummator;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.util.Arrays.asList;

public class Main {

    public static void main(String[] args) {
        Map<String, List<Float>> mapFloat = new HashMap<String, List<Float>>();

        mapFloat.put("1", asList(1.0f, 1.0f, 1.0f));
        mapFloat.put("2", asList(2.0f, 2.0f, 2.0f));
        mapFloat.put("3", asList(3.0f, 3.0f, 3.0f));

        FloatSummator floatSummator = new FloatSummator();
        System.out.println("FloatSummator: " + floatSummator.sum(mapFloat));

        Map<String, List<BigDecimal>>  mapBigDecimal = new HashMap<>();
        BigDecimalSummator bigDecimalSummator = new BigDecimalSummator();

        mapBigDecimal.put("1", asList(new BigDecimal(1.0), new BigDecimal(1.0), new BigDecimal(1.0)));
        mapBigDecimal.put("2", asList(ONE, ONE, ONE));
        mapBigDecimal.put("3", asList(TEN, TEN, TEN));

        System.out.println("BigDecimalSummator: " + bigDecimalSummator.sum(mapBigDecimal));

        ConvertToDouble convertToFloat = new ConvertToDouble();
        ConvertToString convertToInteger = new ConvertToString();

        Float num = 4.0f;
        Integer[] integers = {1, 2, 3, 4, 5};

        System.out.println("Input Float - output Double: " + convertToFloat.get(num) + '-' + convertToFloat.get(num).getClass().getTypeName());
        System.out.println("Input Integer[] - output String: " + convertToInteger.get(integers) + '-' + convertToInteger.get(integers).getClass().getName());

    }
}
