package core.home;

import core.BasePage;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

@Getter
public class HomePage extends BasePage {

    public HomePage(WebDriver webDriver) {
        super(webDriver);
    }

    public HomePage() {
        super();
    }

    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
    private WebElement searchField;

    @FindBy(xpath = "//input[@class='nav-input' and not(@id)]")
    private WebElement searchButton;

    @FindBy(xpath = "//td/a[@class='nav_a']")
    private List<WebElement> footerLinks;

}
