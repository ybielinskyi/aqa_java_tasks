package configuration;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;

public class WebDriverSingleton {
    private static WebDriver driver;

    private WebDriverSingleton() {

    }

    public static WebDriver getInstance() {
        if (driver == null) {
            switch (Config.property.getProperty("browserName")) {
                case "Chrome":
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                    break;

                case "Safari":
                    driver = new SafariDriver();
                    break;
            }

            driver.manage().window().maximize();
        }

        return driver;
    }
}
