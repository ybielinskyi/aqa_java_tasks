package ordinary;

import configuration.WebDriverSingleton;
import core.home.HomeHelper;
import core.search.SearchResultsHelper;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected SoftAssert softAssert;
    protected static WebDriver webDriver;

    @Autowired
    protected static HomeHelper homeHelper;

    @BeforeSuite(enabled = true)
    public void beforeSuiteInit() {
        webDriver = WebDriverSingleton.getInstance();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        softAssert = new SoftAssert();

        ApplicationContext context = new ClassPathXmlApplicationContext("appConfig.xml");
        homeHelper = context.getBean("homeHelper", HomeHelper.class);
        homeHelper.getPage().setWebDriver(webDriver);
    }

    @AfterSuite(enabled = true)
    public void afterSuiteTearDown() {
        webDriver.quit();
    }

}
