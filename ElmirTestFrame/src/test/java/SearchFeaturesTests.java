import org.openqa.selenium.Cookie;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.*;
import java.util.Date;
import java.util.StringTokenizer;

public class SearchFeaturesTests extends BaseTest {

    @Test
    public void redirectToResultsPageTest() {
        Assert.assertTrue(homeHelper
                .open()
                .searchQuery("mouse")
                .getPageTitle()
                .getText()
                .contains("mouse"));
    }

    @Test
    public void itemsCountOnPage() {
        homeHelper
                .open()
                .searchQuery("mouse");

        Assert.assertEquals(searchResultsPage
                .getItemCountDropMenu()
                .getText(), searchResultsPage
                .getItems()
                .size());
    }

    @Test
    public void correctSearch() {
        Assert.assertTrue(homeHelper
                .open()
                .searchQuery("mouse")
                .getItems()
                .get(0)
                .getItemName()
                .toString()
                .toLowerCase()
                .contains("mouse"));
    }
}
