package coreElmir.cart;

import coreElmir.BaseHelper;
import coreElmir.cart.checkoutSteps.Questionnaire;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CartHelper extends BaseHelper<CartPage> {

    public CartHelper(WebDriver webDriver) {
        super(new CartPage(webDriver));
    }

    public Questionnaire checkOutButtonClick() {
        getPage().getCheckoutButton().click();

        return (new Questionnaire(getPage().getWebDriver()));
    }

    public WebElement getCartItems() {
        return getPage().getCartItems();
    }
}
