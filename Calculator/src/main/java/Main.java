import calculator.Calculate;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        Scanner fileIn = new Scanner(new File("Calculator/src/main/resources/inputData.txt"));
        PrintWriter pw = new PrintWriter(new FileWriter("Calculator/src/main/resources/outputData"));

        while (fileIn.hasNextLine()) {
            pw.write(Calculate.evaluate(fileIn.nextLine()) + '\n');
        }
        pw.close();
    }
}
