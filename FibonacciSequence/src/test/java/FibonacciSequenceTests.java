import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FibonacciSequenceTests {

    @Test
    public void validEndSequence() {
        assertEquals(Main.sequence(34), "1 1 2 3 5 8 13 21 34 ");
    }

    @Test
    public void invalidEndSequence() {
        assertEquals(Main.sequence(-34), "Invalid endSequence");

    }
}
