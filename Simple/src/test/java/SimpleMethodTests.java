import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


public class SimpleMethodTests {

    @Test
    public void validInput() {
        assertEquals(Main.simple(1, 10), "1 2 3 5 7 ");
    }

    @Test
    public void reverseInput() {
        assertEquals(Main.simple(10, 1), "1 2 3 5 7 ");
    }

    @Test
    public void zeroInput() {
        assertEquals(Main.simple(0, 0), "");

    }

    @Test
    public void simpleNumInput() {
        assertEquals(Main.simple(5, 5), "5 ");

    }
}
