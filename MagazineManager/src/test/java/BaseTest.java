import magazine.magazineCore.EMagazine;
import magazine.magazineCore.Magazine;
import magazine.magazineCore.PaperMagazine;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class BaseTest {
    protected static final String baseUrl = "/Users/ybielinskyi/IdeaProjects/aqa_java_tasks/MagazineManager/src/test/resources/magazines/";
    protected static List<Magazine> magazinesList;
    protected static ByteArrayOutputStream outContent;

    @BeforeMethod
    public void dataInit() {
        try {
            magazinesList = new ArrayList<>(asList(
                    new PaperMagazine("paperMagazine", "paperMagazine", "testMagazine", "testLang", 0,
                            0, 1, new SimpleDateFormat("yyyy-MM-dd").parse("1999-12-12"), 1, 1),
                    new EMagazine("eMagazine", "eMagazine", "testMagazine", "testLang", 0, 0,
                            1, new SimpleDateFormat("yyyy-MM-dd").parse("2015-12-12"), "PDF", 123)));
        } catch (ParseException e) {
            System.out.println("Parse error");
        }

        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    @AfterMethod
    public void tearDown() {
        System.setOut(null);
    }
}
