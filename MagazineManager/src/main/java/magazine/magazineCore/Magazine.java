package magazine.magazineCore;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jsonProvider.DateDeserializer;
import jsonProvider.DateSerializer;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = EMagazine.class, name = "eMagazine"),
        @JsonSubTypes.Type(value = PaperMagazine.class, name = "paperMagazine")})
public abstract class Magazine implements Serializable {

    @JsonProperty("Type")
    protected String type;

    @JsonProperty("MagazineName")
    protected String magazineName;

    @JsonProperty("Name")
    protected String IssueName;

    @JsonProperty("Language")
    protected String language;

    @JsonProperty("PriceIssue")
    protected double priceIssue;

    @JsonProperty("PricePerYear")
    protected double pricePerYear;

    @JsonProperty("Frequency")
    protected int frequency;

    @JsonProperty("IssueDate")
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    protected Date issueDate;

    public Magazine() {

    }

    public Magazine(String type, String magazineName, String IssueName, String language, double priceIssue, double pricePerYear, int frequency, Date issueDate) {
        this.type = type;
        this.magazineName = magazineName;
        this.IssueName = IssueName;
        this.language = language;
        this.priceIssue = priceIssue;
        this.pricePerYear = pricePerYear;
        this.frequency = frequency;
        this.issueDate = issueDate;
    }

    public void calculateDiscount() {
        if (pricePerYear != 0 && priceIssue != 0) {
            System.out.println("Discount is " + ((priceIssue * frequency) * 12 - pricePerYear));
        } else {
            System.out.println("Issue is FREE");
        }
    }

    public abstract void editIssue();
}