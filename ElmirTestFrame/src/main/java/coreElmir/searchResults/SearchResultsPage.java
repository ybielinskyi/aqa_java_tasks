package coreElmir.searchResults;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import coreElmir.BasePage;

import java.util.List;

@Getter
public class SearchResultsPage extends BasePage {

    public SearchResultsPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//span[@class='current']//span[@class='internal-link'][contains(text(),'24')]")
    private WebElement itemCountDropMenu;

    @FindBy(xpath = "//h1[@id='page-title']")
    private WebElement pageTitle;

    @FindBy(css = "#vitrina-tovars")
    private WebElement resultsDiv;

    @FindBy(css = "li[class='core.item']")
    private List<SearchResultItem> items;

}
